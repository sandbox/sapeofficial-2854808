#Sape.ru integration

Drupal plugin for sape.ru webmaster services integration v0.01
- built in version of sape.ru's client code: 1.3.8

## System requirements
Drupal 8+ [system requirements](https://www.drupal.org/docs/8/system-requirements) 

## Supported features
- back links placement (common and block mode)
- context back links placement
- teaser placement
- advertisement blocks of rtb.sape
- articles